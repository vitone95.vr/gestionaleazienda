package MODEL;

import java.util.Date;
import java.util.Objects;

public class UtenteDTO {

	private int id;
	private double stipendio;
	private String nome;
	private String cognome;
	private String sesso;
	private Date dataNascita;
	private String codiceFiscale;
	private String cittaResidenza;
	private String username;
	private String email;
	private String password;
	private String tipo;
	private int idPermesso;
	private int idPersona;
	private int numprog;
	
	public int getNumprog() {
		return numprog;
	}
	public void setNumprog(int numprog) {
		this.numprog = numprog;
	}
	public String getCittaResidenza() {
		return cittaResidenza;
	}
	public void setCittaResidenza(String cittaResidenza) {
		this.cittaResidenza = cittaResidenza;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getStipendio() {
		return stipendio;
	}
	public void setStipendio(double stipendio) {
		this.stipendio = stipendio;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getSesso() {
		return sesso;
	}
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	public Date getDataNascita() {
		return dataNascita;
	}
	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getIdPermesso() {
		return idPermesso;
	}
	public void setIdPermesso(int idPermesso) {
		this.idPermesso = idPermesso;
	}
	public int getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}
	@Override
	public String toString() {
		return "UtenteDTO [id=" + id + ", stipendio=" + stipendio + ", nome=" + nome + ", cognome=" + cognome
				+ ", sesso=" + sesso + ", dataNascita=" + dataNascita + ", codiceFiscale=" + codiceFiscale
				+ ", cittaResidenza=" + cittaResidenza + ", username=" + username + ", email=" + email + ", password="
				+ password + ", tipo=" + tipo + ", idPermesso=" + idPermesso + ", idPersona=" + idPersona + "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(cittaResidenza, codiceFiscale, cognome, dataNascita, email, id, idPermesso, idPersona, nome,
				password, sesso, stipendio, tipo, username);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UtenteDTO other = (UtenteDTO) obj;
		return Objects.equals(cittaResidenza, other.cittaResidenza)
				&& Objects.equals(codiceFiscale, other.codiceFiscale) && Objects.equals(cognome, other.cognome)
				&& Objects.equals(dataNascita, other.dataNascita) && Objects.equals(email, other.email)
				&& id == other.id && idPermesso == other.idPermesso && idPersona == other.idPersona
				&& Objects.equals(nome, other.nome) && Objects.equals(password, other.password)
				&& Objects.equals(sesso, other.sesso)
				&& Double.doubleToLongBits(stipendio) == Double.doubleToLongBits(other.stipendio)
				&& Objects.equals(tipo, other.tipo) && Objects.equals(username, other.username);
	}
	public UtenteDTO(int id, double stipendio, String nome, String cognome, String sesso, Date dataNascita,
			String codiceFiscale, String cittaResidenza, String username, String email, String password, String tipo,
			int idPermesso, int idPersona) {
		this.id = id;
		this.stipendio = stipendio;
		this.nome = nome;
		this.cognome = cognome;
		this.sesso = sesso;
		this.dataNascita = dataNascita;
		this.codiceFiscale = codiceFiscale;
		this.cittaResidenza = cittaResidenza;
		this.username = username;
		this.email = email;
		this.password = password;
		this.tipo = tipo;
		this.idPermesso = idPermesso;
		this.idPersona = idPersona;
		
	}
	public UtenteDTO(int id, double stipendio, String nome, String cognome, String sesso, Date dataNascita,
			String codiceFiscale, String cittaResidenza, String username, String email, String password, String tipo,
			int idPermesso, int idPersona, int numprog) {
		this.id = id;
		this.stipendio = stipendio;
		this.nome = nome;
		this.cognome = cognome;
		this.sesso = sesso;
		this.dataNascita = dataNascita;
		this.codiceFiscale = codiceFiscale;
		this.cittaResidenza = cittaResidenza;
		this.username = username;
		this.email = email;
		this.password = password;
		this.tipo = tipo;
		this.idPermesso = idPermesso;
		this.idPersona = idPersona;
		this.numprog= numprog;
	}
	public UtenteDTO( String nome, String cognome, String sesso, Date dataNascita,
			String codiceFiscale, String cittaResidenza,double stipendio, String username, String email, String password,
			int idPermesso) {
		this.nome = nome;
		this.cognome = cognome;
		this.sesso = sesso;
		this.dataNascita = dataNascita;
		this.codiceFiscale = codiceFiscale;
		this.cittaResidenza = cittaResidenza;
		this.stipendio = stipendio;
		this.username = username;
		this.email = email;
		this.password = password;
		this.idPermesso = idPermesso;
	}
	public UtenteDTO(int id, double stipendio, String nome, String cognome, String email) {
		this.id=id;
		this.stipendio = stipendio;
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
	}
	public UtenteDTO() {
		
	}
	
}
