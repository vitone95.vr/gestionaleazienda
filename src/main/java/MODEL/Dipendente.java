package MODEL;

import java.sql.Date;
import java.util.Objects;

public class Dipendente extends Persona {
	
	private int id;
	private double stipendio;
	private int idPersona;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}

	public double getStipendio() {
		return stipendio;
	}

	public void setStipendio(double stipendio) {
		this.stipendio = stipendio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(id, idPersona, stipendio);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dipendente other = (Dipendente) obj;
		return id == other.id && idPersona == other.idPersona
				&& Double.doubleToLongBits(stipendio) == Double.doubleToLongBits(other.stipendio);
	}

	@Override
	public String toString() {
		return "Dipendente [ stipendio=" + stipendio + ", getNome()=" + getNome() + ", getCognome()=" + getCognome()
				+ ", getSesso()=" + getSesso() + ", getDataNascita()=" + getDataNascita() + ", getCodiceFiscale()="
				+ getCodiceFiscale() + ", getCittaResidenza()=" + getCittaResidenza() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + "]";
	}

	public Dipendente(int id, String nome, String cognome, String sesso, Date dataNascita, String codiceFiscale,
			String cittaResidenza, double stipendio) {
		super(nome, cognome, sesso, dataNascita, codiceFiscale, cittaResidenza);
		this.id= id;
		this.stipendio = stipendio;
	}

	public Dipendente() {
		
	}

}
