package MODEL;

import java.util.Objects;

public class Progetto {
	
	private int id;
	private String nomeProgetto;
	private String descrizione;
	private String linkimg;
	private double costo;
	private int numdip;
	
	
	public int getNumdip() {
		return numdip;
	}
	public void setNumdip(int numdip) {
		this.numdip = numdip;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomeProgetto() {
		return nomeProgetto;
	}
	public void setNomeProgetto(String nomeProgetto) {
		this.nomeProgetto = nomeProgetto;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getLinkimg() {
		return linkimg;
	}
	public void setLinkimg(String linkimg) {
		this.linkimg = linkimg;
	}
	public double getCosto() {
		return costo;
	}
	public void setCosto(double costo) {
		this.costo = costo;
	}
	@Override
	public int hashCode() {
		return Objects.hash(costo, descrizione, id, linkimg, nomeProgetto);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Progetto other = (Progetto) obj;
		return Double.doubleToLongBits(costo) == Double.doubleToLongBits(other.costo)
				&& Objects.equals(descrizione, other.descrizione) && id == other.id
				&& Objects.equals(linkimg, other.linkimg) && Objects.equals(nomeProgetto, other.nomeProgetto);
	}
	public Progetto(int id, String nomeProgetto, String descrizione, String linkimg, double costo, int numdip) {
		this.id = id;
		this.nomeProgetto = nomeProgetto;
		this.descrizione = descrizione;
		this.linkimg = linkimg;
		this.costo = costo;
		this.numdip = numdip;
	}
	public Progetto(int id, String nomeProgetto, String descrizione, String linkimg, double costo) {
		this.id = id;
		this.nomeProgetto = nomeProgetto;
		this.descrizione = descrizione;
		this.linkimg = linkimg;
		this.costo = costo;
	}
	public Progetto() {
		
	}
}
