package MODEL;

import java.util.Objects;

public class Permesso {

	private int id;
	private String tipo;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id= id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Permesso [tipo=" + tipo + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(tipo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permesso other = (Permesso) obj;
		return Objects.equals(tipo, other.tipo);
	}

	public Permesso(String tipo) {
		this.tipo = tipo;
	}
	
	public Permesso() {
		
	}
}
