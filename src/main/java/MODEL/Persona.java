package MODEL;


import java.util.Date;
import java.util.Objects;

public class Persona {

	private int id;
	private String nome;
	private String cognome;
	private String sesso;
	private Date dataNascita;
	private String codiceFiscale;
	private String cittaResidenza;
	
	public int getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getSesso() {
		return sesso;
	}
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	public Date getDataNascita() {
		return dataNascita;
	}
	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	public String getCittaResidenza() {
		return cittaResidenza;
	}
	public void setCittaResidenza(String cittaResidenza) {
		this.cittaResidenza = cittaResidenza;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(cittaResidenza, codiceFiscale, cognome, dataNascita, nome, sesso);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		return Objects.equals(cittaResidenza, other.cittaResidenza)
				&& Objects.equals(codiceFiscale, other.codiceFiscale) && Objects.equals(cognome, other.cognome)
				&& Objects.equals(dataNascita, other.dataNascita) && Objects.equals(nome, other.nome)
				&& Objects.equals(sesso, other.sesso);
	}
	
	@Override
	public String toString() {
		return "Persona [nome=" + nome + ", cognome=" + cognome + ", sesso=" + sesso + ", dataNascita=" + dataNascita
				+ ", codiceFiscale=" + codiceFiscale + ", cittaResidenza=" + cittaResidenza + "]";
	}
	
	public Persona(String nome, String cognome, String sesso, Date dataNascita, String codiceFiscale,
			String cittaResidenza) {
		this.nome = nome;
		this.cognome = cognome;
		this.sesso = sesso;
		this.dataNascita = dataNascita;
		this.codiceFiscale = codiceFiscale;
		this.cittaResidenza = cittaResidenza;
	}
	
	public Persona() {
		
	}

}
