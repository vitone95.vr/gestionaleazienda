package MODEL;

public class Validator {

	public static boolean isNumber(String value) {
		try {
			if (value != null)
				Double.parseDouble(value);
			else
				return false;
		} catch (NumberFormatException e) {
			return false;
		}
		
		return true;
	}
	
}
