package MODEL;

import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Crud {

	public final static String DB_DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";
	public final static String DB_URL="jdbc:mysql://localhost:3307/dbutenti";
	public final static String DB_USERNAME = "root";
	public final static String DB_PASSWORD = "";
	static Connection conn=null;	

	static
	{
		try {
			Class.forName(DB_DRIVER_CLASS);
			conn = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}	
	}

	public int inserimento(Persona p) 
	{
		PreparedStatement ps = null;
		String sql = "INSERT INTO persone (nome,cognome,sesso,dataNascita,codiceFiscale,cittaResidenza) VALUES (?,?,?,?,?,?)";

		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNome());
			ps.setString(2, p.getCognome());
			ps.setString(3, p.getSesso());
			/* ps.setDate(4, p.getDataNascita()); */
			ps.setString(5, p.getCodiceFiscale());
			ps.setString(6, p.getCittaResidenza());
			
			  SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			  String formattedDate = simpleDateFormat.format(p.getDataNascita());
			  java.sql.Date date1 = java.sql.Date.valueOf(formattedDate);
			  ps.setDate(4,date1);
			 
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    return 0;
	}
	
	public int inserimento(Dipendente d, int idp) 
	{
		PreparedStatement ps = null;
		String sql = "INSERT INTO dipendenti (stipendio,idPersona) VALUES (?,?)";

		try {
			ps = conn.prepareStatement(sql);
			ps.setDouble(1, d.getStipendio());
			ps.setInt(2, idp);
			
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    return 0;
	}
	
	public int inserimento(Account a, int idp) 
	{
		PreparedStatement ps = null;
		String sql = "INSERT INTO accounts (username,email,password,idPermesso,idPersona) VALUES (?,?,?,?,?)";
		
		try {
			
			ps = conn.prepareStatement(sql);
			ps.setString(1, a.getUsername());
			ps.setString(2, a.getEmail());
			ps.setString(3, a.getPassword());
			ps.setInt(4, a.getIdPermesso());
			ps.setInt(5, idp);
			
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    return 0;
	}
	
	public Account cercausername(String username,String password) {
		ResultSet rs;
		PreparedStatement ps;
		Account a =null;
		String sql = "SELECT * FROM `accounts` WHERE (username=? AND password=?) OR (email=? AND password=?);";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			ps.setString(3, username);
			ps.setString(4, password);
			rs=ps.executeQuery();
			while(rs.next())
			{
			a = new Account(
				rs.getString("accounts.username"),
				rs.getString("accounts.password"),
				rs.getString("accounts.email"),
				rs.getInt("accounts.idPermesso"),
				rs.getInt("accounts.idPersona")
			);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return a;
	}
	
	public ArrayList<UtenteDTO> ricercautenti() {
		ResultSet rs;
		PreparedStatement ps;
		ArrayList<UtenteDTO> utenti = new ArrayList<>();
		UtenteDTO u= null;
		String sql = "SELECT * FROM `dipendenti`"
				   + "INNER JOIN persone ON (dipendenti.idPersona=persone.id)"
				   + "INNER JOIN accounts ON (persone.id=accounts.idPersona)"
				   + "INNER JOIN permessi ON (accounts.idPermesso=permessi.id)";
		
		try {
	        ps = conn.prepareStatement(sql);
	        rs = ps.executeQuery();
	        
	        while (rs.next()) {
	            u= new UtenteDTO();
	            u.setId(rs.getInt("dipendenti.id"));
	            u.setNome(rs.getString("persone.nome"));
	            u.setCognome(rs.getString("persone.cognome"));
	            u.setSesso(rs.getString("persone.sesso"));
	            u.setDataNascita(rs.getDate("persone.dataNascita"));
	            u.setCodiceFiscale(rs.getString("persone.codiceFiscale"));
	            u.setCittaResidenza(rs.getString("persone.cittaResidenza"));
	            u.setStipendio(rs.getDouble("dipendenti.stipendio"));
	            u.setUsername(rs.getString("accounts.username"));
	            u.setPassword(rs.getString("accounts.password"));
	            u.setEmail(rs.getString("accounts.email"));
	            u.setIdPermesso(rs.getInt("accounts.idPermesso"));
	            u.setTipo(rs.getString("permessi.tipo"));
	            u.setNumprog(numprog(u.getId()));
	            utenti.add(u);
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
		return utenti;
	}

	public ArrayList<UtenteDTO> ricercautenti(int limit,int offset) {
		ResultSet rs;
		PreparedStatement ps;
		ArrayList<UtenteDTO> utenti = new ArrayList<>();
		UtenteDTO u= null;
		String sql = "SELECT * FROM `dipendenti` "
				   + "INNER JOIN persone ON (dipendenti.idPersona=persone.id) "
				   + "INNER JOIN accounts ON (persone.id=accounts.idPersona) "
				   + "INNER JOIN permessi ON (accounts.idPermesso=permessi.id) "
				   + "LIMIT ? "
				   + "OFFSET ? ";
		
		try {
	        ps = conn.prepareStatement(sql);
	        ps.setInt(1, limit);
	        ps.setInt(2, offset);
	        rs = ps.executeQuery();
	        
	        while (rs.next()) {
	            u= new UtenteDTO();
	            u.setId(rs.getInt("dipendenti.id"));
	            u.setNome(rs.getString("persone.nome"));
	            u.setCognome(rs.getString("persone.cognome"));
	            u.setSesso(rs.getString("persone.sesso"));
	            u.setDataNascita(rs.getDate("persone.dataNascita"));
	            u.setCodiceFiscale(rs.getString("persone.codiceFiscale"));
	            u.setCittaResidenza(rs.getString("persone.cittaResidenza"));
	            u.setStipendio(rs.getDouble("dipendenti.stipendio"));
	            u.setUsername(rs.getString("accounts.username"));
	            u.setPassword(rs.getString("accounts.password"));
	            u.setEmail(rs.getString("accounts.email"));
	            u.setIdPermesso(rs.getInt("accounts.idPermesso"));
	            u.setTipo(rs.getString("permessi.tipo"));
	            u.setNumprog(numprog(u.getId()));
	            utenti.add(u);
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
		return utenti;
	}
	
	public int numprog(int id) {
		
		ResultSet rs;
		PreparedStatement ps;
		String sql= "SELECT COUNT(idProgetto) as numprog from workings where idDipendente=?";
		int num=0;
		
		try {
			ps= conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs=ps.executeQuery();
			
			if(rs.next()) {
				num=rs.getInt("numprog");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}
	
	public ArrayList<UtenteDTO> ricercautenti(String cf) {
		ResultSet rs;
		PreparedStatement ps;
		ArrayList<UtenteDTO> utenti = new ArrayList<>();
		UtenteDTO u= null;
		String sql = "SELECT * FROM `dipendenti`"
				   + "INNER JOIN persone ON (dipendenti.idPersona=persone.id)"
				   + "INNER JOIN accounts ON (persone.id=accounts.idPersona)"
				   + "INNER JOIN permessi ON (accounts.idPermesso=permessi.id)"
				   + "WHERE codiceFiscale=?";
		
		try {
	        ps = conn.prepareStatement(sql);
	        ps.setString(1, cf);
	        rs = ps.executeQuery();
	        
	        while (rs.next()) {
	            u= new UtenteDTO();
	            u.setNome(rs.getString("persone.nome"));
	            u.setCognome(rs.getString("persone.cognome"));
	            u.setSesso(rs.getString("persone.sesso"));
	            u.setDataNascita(rs.getDate("persone.dataNascita"));
	            u.setCodiceFiscale(rs.getString("persone.codiceFiscale"));
	            u.setCittaResidenza(rs.getString("persone.cittaResidenza"));
	            u.setStipendio(rs.getDouble("dipendenti.stipendio"));
	            u.setUsername(rs.getString("accounts.username"));
	            u.setPassword(rs.getString("accounts.password"));
	            u.setEmail(rs.getString("accounts.email"));
	            u.setTipo(rs.getString("permessi.tipo"));
	            utenti.add(u);
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
		return utenti;
	}

	public ArrayList<UtenteDTO> ricercautenti2(String cr) {
		ResultSet rs;
		PreparedStatement ps;
		ArrayList<UtenteDTO> utenti = new ArrayList<>();
		UtenteDTO u= null;
		String sql = "SELECT * FROM `dipendenti`"
				   + "INNER JOIN persone ON (dipendenti.idPersona=persone.id)"
				   + "INNER JOIN accounts ON (persone.id=accounts.idPersona)"
				   + "INNER JOIN permessi ON (accounts.idPermesso=permessi.id)"
				   + "WHERE cittaResidenza=?";
		
		try {
	        ps = conn.prepareStatement(sql);
	        ps.setString(1, cr);
	        rs = ps.executeQuery();
	        
	        while (rs.next()) {
	            u= new UtenteDTO();
	            u.setNome(rs.getString("persone.nome"));
	            u.setCognome(rs.getString("persone.cognome"));
	            u.setSesso(rs.getString("persone.sesso"));
	            u.setDataNascita(rs.getDate("persone.dataNascita"));
	            u.setCodiceFiscale(rs.getString("persone.codiceFiscale"));
	            u.setCittaResidenza(rs.getString("persone.cittaResidenza"));
	            u.setStipendio(rs.getDouble("dipendenti.stipendio"));
	            u.setUsername(rs.getString("accounts.username"));
	            u.setPassword(rs.getString("accounts.password"));
	            u.setEmail(rs.getString("accounts.email"));
	            u.setTipo(rs.getString("permessi.tipo"));
	            utenti.add(u);
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
		return utenti;
	}

	public ArrayList<UtenteDTO> ricercautenti3(String nome,String cognome) {
		ResultSet rs;
		PreparedStatement ps;
		ArrayList<UtenteDTO> utenti = new ArrayList<>();
		UtenteDTO u= null;
		String sql = "SELECT * FROM `dipendenti`"
				   + "INNER JOIN persone ON (dipendenti.idPersona=persone.id)"
				   + "INNER JOIN accounts ON (persone.id=accounts.idPersona)"
				   + "INNER JOIN permessi ON (accounts.idPermesso=permessi.id)"
				   + "WHERE nome=? AND cognome=?";
		
		try {
	        ps = conn.prepareStatement(sql);
	        ps.setString(1, nome);
	        ps.setString(2, cognome);
	        rs = ps.executeQuery();
	        
	        while (rs.next()) {
	            u= new UtenteDTO();
	            u.setNome(rs.getString("persone.nome"));
	            u.setCognome(rs.getString("persone.cognome"));
	            u.setSesso(rs.getString("persone.sesso"));
	            u.setDataNascita(rs.getDate("persone.dataNascita"));
	            u.setCodiceFiscale(rs.getString("persone.codiceFiscale"));
	            u.setCittaResidenza(rs.getString("persone.cittaResidenza"));
	            u.setStipendio(rs.getDouble("dipendenti.stipendio"));
	            u.setUsername(rs.getString("accounts.username"));
	            u.setPassword(rs.getString("accounts.password"));
	            u.setEmail(rs.getString("accounts.email"));
	            u.setTipo(rs.getString("permessi.tipo"));
	            utenti.add(u);
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
		return utenti;
	}
	
	public UtenteDTO ricercautente(String cf) {
		ResultSet rs;
		PreparedStatement ps;
		UtenteDTO u = null;
		String sql = "SELECT * FROM `dipendenti`"
				   + "INNER JOIN persone ON (dipendenti.idPersona=persone.id)"
				   + "INNER JOIN accounts ON (persone.id=accounts.idPersona)"
				   + "INNER JOIN permessi ON (accounts.idPermesso=permessi.id)"
				   + "WHERE codiceFiscale=?";
		
		try {
	        ps = conn.prepareStatement(sql);
	        ps.setString(1, cf);
	        rs = ps.executeQuery();
	        
	        while (rs.next()) {
	            u = new UtenteDTO(
	            rs.getInt("accounts.id"),		
	            rs.getDouble("dipendenti.stipendio"),
	            rs.getString("persone.nome"),
	            rs.getString("persone.cognome"),
	            rs.getString("persone.sesso"),
	            rs.getDate("persone.dataNascita"),
	            rs.getString("persone.codiceFiscale"),
	            rs.getString("persone.cittaResidenza"),
	            rs.getString("accounts.username"),
	            rs.getString("accounts.email"),
	            rs.getString("accounts.password"),
	            rs.getString("permessi.tipo"),
	            rs.getInt("accounts.idPermesso"),
	            rs.getInt("accounts.idPersona")
	            );
	            
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
		return u;
	}
	
	public Persona ricerca(String cf)
	{
		ResultSet rs;
		PreparedStatement ps;
		Persona p =null;
		String sql = "SELECT * from persone where codiceFiscale=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, cf);
			//commit
			rs=ps.executeQuery();
			while(rs.next())
			{
				p = new Persona(
						rs.getString("persone.nome"),
						rs.getString("persone.cognome"),
						rs.getString("persone.sesso"),
						rs.getDate("persone.dataNascita"),
						rs.getString("persone.codiceFiscale"),
						rs.getString("persone.cittaResidenza")
						);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}
	
	public int ricercaId(String cf) {
		ResultSet rs;
		PreparedStatement ps;
		int id=0;
		String sql = "SELECT id from persone where codiceFiscale=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, cf);
			rs=ps.executeQuery();
		      
	        if (rs.next()) {
	            id = rs.getInt("persone.id");
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
	}
	
	public int modifica(String cf,UtenteDTO u) {
		
		PreparedStatement ps = null;
		int i=0;
		String sql =  "UPDATE persone p,dipendenti d,accounts a SET p.nome=?,p.cognome=?,p.sesso=?,p.dataNascita=?,p.codiceFiscale=?,p.cittaResidenza=?,"
					+ "d.stipendio=?,a.username=?,a.email=?,a.password=?,a.idPermesso=? where p.codiceFiscale=? AND p.id=d.idPersona AND p.id=a.idPersona";
		/*
		 * UPDATE persone p ,dipendenti d ,accounts a SET
		 * p.nome='vito',p.cognome='pipo',p.sesso='m',p.dataNascita='2023-01-01',p.
		 * codiceFiscale='abc',p.cittaResidenza='ba',
		 * d.stipendio='1234',a.username='asd',a.email='ad@ad.com',a.password='wee',a.
		 * idPermesso='2' where p.codiceFiscale=123 AND p.id=d.idPersona AND
		 * p.id=a.idPersona
		 */
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, u.getNome());
			ps.setString(2, u.getCognome());
			ps.setString(3, u.getSesso());
			/* ps.setDate(4, d.getDataNascita()); */
			ps.setString(5, u.getCodiceFiscale());
			ps.setString(6, u.getCittaResidenza());
			ps.setDouble(7, u.getStipendio());
			ps.setString(8, u.getUsername());
			ps.setString(9, u.getEmail());
			ps.setString(10, u.getPassword());
			ps.setInt(11, u.getIdPermesso());
			ps.setString(12, cf);
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String formattedDate = simpleDateFormat.format(u.getDataNascita());
			java.sql.Date date1 = java.sql.Date.valueOf(formattedDate);
			ps.setDate(4, date1);
			
			i=ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	}
	
	public int elimina(Persona p) {
		
		PreparedStatement ps=null;
		String sql = "DELETE FROM persone WHERE codiceFiscale=?;";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getCodiceFiscale());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public Date inserisciData(String str) {
		Date date=null;
		SimpleDateFormat dateParser= new SimpleDateFormat("yyyy/MM/dd");
		str=str.replace("-","/");
		try {
			date= dateParser.parse(str);
		}catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public ArrayList<Dipendente> recuperadip()
	{
		ArrayList<Dipendente> array=new ArrayList<Dipendente>();
		String sql = "SELECT * FROM dipendenti";
		ResultSet set;
		Dipendente d;
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement(sql);
			set=ps.executeQuery();
			while(set.next())
			{
				d=new Dipendente();
				d.setNome(set.getString("nome"));
				d.setCognome(set.getString("cognome"));
				d.setSesso(set.getString("sesso"));
				d.setDataNascita(set.getDate("dataNascita"));
				d.setCodiceFiscale(set.getString("codiceFiscale"));
				d.setCittaResidenza(set.getString("cittaResidenza"));
				d.setStipendio(set.getDouble("stipendio"));
				array.add(d);	
			}
			ps.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public ArrayList<Account> recuperaacc(){
		ArrayList<Account> array=new ArrayList<Account>();
		String sql = "SELECT * FROM accounts";
		ResultSet set;
		Account a;
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement(sql);
			set=ps.executeQuery();
			while(set.next())
			{
				a=new Account();
				a.setUsername(set.getString("username"));
				a.setEmail(set.getString("email"));
				a.setPassword(set.getString("password"));
				a.setIdPermesso(set.getInt("idPermesso"));
				a.setIdPersona(set.getInt("idPersona"));
				array.add(a);	
			}
			ps.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public ArrayList<Permesso> recuperaperm(){
		
		ArrayList<Permesso> array=new ArrayList<Permesso>();
		String sql = "SELECT * FROM permessi";
		ResultSet set;
		Permesso p;
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement(sql);
			set=ps.executeQuery();
			while(set.next())
			{
				p=new Permesso();
				p.setId(set.getInt("id"));
				p.setTipo(set.getString("tipo"));
				array.add(p);	
			}
			ps.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return array;
	}
		
}
