package MODEL;

import java.util.Objects;

public class Account {

	private String username;
	private String email;
	private String password;
	private int idPermesso;
	private int idPersona;
	
	public int getIdPermesso() {
		return idPermesso;
	}
	public void setIdPermesso(int idPermesso) {
		this.idPermesso = idPermesso;
	}
	public int getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "Account [username=" + username + ", email=" + email + ", password=" + password + ", idPermesso="
				+ idPermesso + ", idPersona=" + idPersona + "]";
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(email, idPermesso, idPersona, password, username);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		return Objects.equals(email, other.email) && idPermesso == other.idPermesso && idPersona == other.idPersona
				&& Objects.equals(password, other.password) && Objects.equals(username, other.username);
	}
	
	public Account(String username, String email, String password, int idPermesso, int idPersona) {
		this.username = username;
		this.email = email;
		this.password = password;
		this.idPermesso = idPermesso;
		this.idPersona = idPersona;
	}
	public Account() {
		
	}
	
}
