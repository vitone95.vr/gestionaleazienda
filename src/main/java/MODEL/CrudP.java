package MODEL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CrudP {

	public final static String DB_DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";
	public final static String DB_URL="jdbc:mysql://localhost:3307/dbutenti";
	public final static String DB_USERNAME = "root";
	public final static String DB_PASSWORD = "";
	static Connection conn=null;	

	static
	{
		try {
			Class.forName(DB_DRIVER_CLASS);
			conn = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}	
	}
	
	public int inserimento(Progetto p) 
	{
		PreparedStatement ps = null;
		int i=0;
		String sql = "INSERT INTO progetti (nomeProgetto,descrizione,linkimg,costo) VALUES (?,?,?,?)";

		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNomeProgetto());
			ps.setString(2, p.getDescrizione());
			ps.setString(3, p.getLinkimg());
			ps.setDouble(4, p.getCosto());
			
			i=ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    return i;
	}
	
	public ArrayList<Progetto> recuperaprog(){
		
		ArrayList<Progetto> array=new ArrayList<Progetto>();
		String sql = "SELECT * FROM progetti";
		ResultSet set;
		Progetto p;
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement(sql);
			set=ps.executeQuery();
			while(set.next())
			{
				p=new Progetto();
				p.setId(set.getInt("id"));
				p.setNomeProgetto(set.getString("nomeProgetto"));
				p.setDescrizione(set.getString("descrizione"));
				p.setLinkimg(set.getString("linkimg"));
				p.setCosto(set.getDouble("costo"));
				p.setNumdip(numdip(p.getId()));
				array.add(p);	
			}
			ps.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public ArrayList<Progetto> recuperaprog(int limit,int offset){
		
		ArrayList<Progetto> array=new ArrayList<Progetto>();
		String sql = "SELECT * FROM progetti LIMIT ? OFFSET ?";
		ResultSet set;
		Progetto p;
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, limit);
			ps.setInt(2, offset);
			set=ps.executeQuery();
			while(set.next())
			{
				p=new Progetto();
				p.setId(set.getInt("id"));
				p.setNomeProgetto(set.getString("nomeProgetto"));
				p.setDescrizione(set.getString("descrizione"));
				p.setLinkimg(set.getString("linkimg"));
				p.setCosto(set.getDouble("costo"));
				p.setNumdip(numdip(p.getId()));
				array.add(p);	
			}
			ps.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public int numdip(int id) {
		
		ResultSet rs;
		PreparedStatement ps;
		String sql= "SELECT COUNT(idDipendente) as numdip from workings where idProgetto=?";
		int num=0;
		
		try {
			ps= conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs=ps.executeQuery();
			
			if(rs.next()) {
				num=rs.getInt("numdip");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}
	
	public Progetto ricerca(String n)
	{
		ResultSet rs;
		PreparedStatement ps;
		Progetto p =null;
		String sql = "SELECT * from progetti where nomeProgetto=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, n);
			rs=ps.executeQuery();
			while(rs.next())
			{
				p = new Progetto(
						rs.getInt("id"),
						rs.getString("nomeProgetto"),
						rs.getString("descrizione"),
						rs.getString("linkimg"),
						rs.getDouble("costo")
						);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}

	public Progetto ricerca(int n)
	{
		ResultSet rs;
		PreparedStatement ps;
		Progetto p =null;
		String sql = "SELECT * from progetti where id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, n);
			rs=ps.executeQuery();
			while(rs.next())
			{
				p = new Progetto(
						rs.getInt("id"),
						rs.getString("nomeProgetto"),
						rs.getString("descrizione"),
						rs.getString("linkimg"),
						rs.getDouble("costo")
						);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}
	
	public int elimina(Progetto p) {
		
		PreparedStatement ps=null;
		String sql = "DELETE FROM progetti WHERE nomeProgetto=?;";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNomeProgetto());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int modifica(String n,Progetto p) {
		
		PreparedStatement ps=null;
		int i=0;
		String sql = "UPDATE progetti SET nomeProgetto=?,descrizione=?,linkimg=?,costo=? WHERE nomeProgetto=?"; 
		
		try {
			ps=conn.prepareStatement(sql);
			ps.setString(1, p.getNomeProgetto());
			ps.setString(2, p.getDescrizione());
			ps.setString(3, p.getLinkimg());
			ps.setDouble(4, p.getCosto());
			ps.setString(5, n);
			i=ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	
	}

	public ArrayList<UtenteDTO> recuperadipin(String n){
		
		ResultSet set;
		PreparedStatement ps;
		ArrayList<UtenteDTO> dip = new ArrayList<>();
		UtenteDTO d;
		String sql= "SELECT dipendenti.id,persone.nome,persone.cognome,dipendenti.stipendio,accounts.email FROM accounts "
				  + "INNER JOIN persone ON (persone.id=accounts.idPersona) "
				  + "INNER JOIN dipendenti ON (persone.id=dipendenti.idPersona) "
				  + "WHERE dipendenti.id IN (SELECT workings.idDipendente FROM progetti "
				  + "INNER JOIN workings ON (progetti.id = workings.idProgetto) WHERE progetti.nomeProgetto=?)";
		
		try {
			ps=conn.prepareStatement(sql);
			ps.setString(1, n);
			set=ps.executeQuery();
			while(set.next()) {
				d= new UtenteDTO();
				d.setId(set.getInt("id"));
				d.setNome(set.getString("nome"));
				d.setCognome(set.getString("cognome"));
				d.setStipendio(set.getDouble("stipendio"));
				d.setEmail(set.getString("email"));
				dip.add(d);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dip;
	}
	
	public ArrayList<UtenteDTO> recuperadipout(String n){
			
			ResultSet set;
			PreparedStatement ps;
			ArrayList<UtenteDTO> dip = new ArrayList<>();
			UtenteDTO d;
			String sql= "SELECT dipendenti.id,persone.nome,persone.cognome,dipendenti.stipendio,accounts.email  FROM accounts "
					  + "INNER JOIN persone ON (persone.id=accounts.idPersona) "
					  + "INNER JOIN dipendenti ON (persone.id=dipendenti.idPersona) "
					  + "WHERE dipendenti.id NOT IN (SELECT workings.idDipendente FROM progetti "
					  + "INNER JOIN workings ON (progetti.id = workings.idProgetto) WHERE progetti.nomeProgetto=?)";
			
			try {
				ps=conn.prepareStatement(sql);
				ps.setString(1, n);
				set=ps.executeQuery();
				while(set.next()) {
					d= new UtenteDTO();
					d.setId(set.getInt("id"));
					d.setNome(set.getString("nome"));
					d.setCognome(set.getString("cognome"));
					d.setStipendio(set.getDouble("stipendio"));
					d.setEmail(set.getString("email"));
					dip.add(d);
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return dip;
		}
	
	public int associa(int idd,int idp) {
		PreparedStatement ps = null;
		int i=0;
		String sql = "INSERT INTO workings (idDipendente,idProgetto) VALUES (?,?)";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, idd);
			ps.setInt(2, idp);
			
			i=ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    return i;
	}
	
	public int dissocia(int idd,int idp) {	
		PreparedStatement ps=null;
		int i=0;
		String sql = "DELETE FROM workings WHERE idProgetto=? AND idDipendente=?;";
			
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, idp);
			ps.setInt(2, idd);
			i=ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	}
	
	public ArrayList<Progetto> ricercacosto(int c) {
		ResultSet rs;
		PreparedStatement ps;
		ArrayList<Progetto> progetti = new ArrayList<>();
		Progetto p= null;
		String sql = "SELECT * FROM progetti WHERE costo>=?";
		
		try {
	        ps = conn.prepareStatement(sql);
	        ps.setInt(1, c);
	        rs = ps.executeQuery();
	        
	        while (rs.next()) {
	            p= new Progetto();
		           p.setId(rs.getInt("id"));
		           p.setNomeProgetto(rs.getString("nomeProgetto"));
		           p.setDescrizione(rs.getString("descrizione"));
		           p.setLinkimg(rs.getString("linkimg"));
				   p.setCosto(rs.getDouble("costo"));
				   p.setNumdip(numdip(p.getId()));
				   progetti.add(p);
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
		return progetti;
	}
}
