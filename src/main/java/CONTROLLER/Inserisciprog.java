package CONTROLLER;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import MODEL.CrudP;
import MODEL.Progetto;

/**
 * Servlet implementation class Inserisciprog
 */
@WebServlet("/Inserisciprog")
public class Inserisciprog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Inserisciprog() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		CrudP c=new CrudP();
		Progetto p=new Progetto();
		p.setNomeProgetto(request.getParameter("nomeProgetto"));
		p.setDescrizione(request.getParameter("descrizione"));
		p.setLinkimg(request.getParameter("linkimg"));
		p.setCosto(Double.parseDouble(request.getParameter("costo")));
		
		if(c.inserimento(p)>0) {
			RequestDispatcher rd=request.getRequestDispatcher("Gestisciprog"); 
			request.setAttribute("msg", "Progetto inserito correttarmente!");
		    rd.forward(request, response);
		}else {
			RequestDispatcher rd=request.getRequestDispatcher("gestioneazienda.jsp"); 
			request.setAttribute("msg", "qualcosa è andato storto");
		    rd.forward(request, response); 
		}
	}

}
