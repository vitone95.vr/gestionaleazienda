package CONTROLLER;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import MODEL.CrudP;

/**
 * Servlet implementation class Associa
 */
@WebServlet("/Associa")
public class Associa extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Associa() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		CrudP c= new CrudP();
		int iddip= Integer.parseInt(request.getParameter("iddip"));
		int idprog= Integer.parseInt(request.getParameter("idprog"));
		//Progetto p= c.ricerca(idprog);
		//ArrayList<UtenteDTO> dipin = c.recuperadipin(p.getNomeProgetto());
		//ArrayList<UtenteDTO> dipout = c.recuperadipout(p.getNomeProgetto());
		
		if(c.associa(iddip, idprog)>0) {
			//request.setAttribute("prog", p);
			//request.setAttribute("dipin", dipin);
			//request.setAttribute("dipout", dipout);
			//request.setAttribute("nomeProgetto", p.getNomeProgetto());
			RequestDispatcher rd = request.getRequestDispatcher("Dettagliprog"); 
		    rd.forward(request, response); 
		}else {
			//request.setAttribute("prog", p);
			//request.setAttribute("dipin", dipin);
			//request.setAttribute("dipout", dipout);
			//request.setAttribute("nomeProgetto", p.getNomeProgetto());
			request.setAttribute("msg", "qualcosa è andato storto");
			RequestDispatcher rd = request.getRequestDispatcher("Dettagliprog"); 
		    rd.forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
