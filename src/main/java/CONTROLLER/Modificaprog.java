package CONTROLLER;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import MODEL.CrudP;
import MODEL.Progetto;

/**
 * Servlet implementation class Modificaprog
 */
@WebServlet("/Modificaprog")
public class Modificaprog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Modificaprog() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session=request.getSession();
		CrudP c= new CrudP();
		String n= request.getParameter("nomeProgetto");
		Progetto p=c.ricerca(n);
		session.getAttribute("login");
		
		if(p!=null) {
			RequestDispatcher rd = request.getRequestDispatcher("insprog.jsp");
			request.setAttribute("prog", p);
			rd.forward(request,response);
		}else {
			RequestDispatcher rd = request.getRequestDispatcher("Gestisciprog");
			session.setAttribute("msgm", "qualcosa è andato storto");
			rd.forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String n= request.getParameter("oldn");
		CrudP c= new CrudP();
		Progetto p=new Progetto();
		
		p.setNomeProgetto(request.getParameter("nomeProgetto"));
		p.setDescrizione(request.getParameter("descrizione"));
		p.setLinkimg(request.getParameter("linkimg"));
		p.setCosto(Double.parseDouble(request.getParameter("costo")));
		
		if(c.modifica(n,p)>0) {
			RequestDispatcher rd=request.getRequestDispatcher("Gestisciprog"); 
			request.setAttribute("msgm", "Utente modificato correttamente!");
		    rd.forward(request, response);
		}else {
			RequestDispatcher rd=request.getRequestDispatcher("Gestisciprog"); 
			request.setAttribute("msgm", "qualcosa è andato storto");
		    rd.forward(request, response);
		}
	}

}
