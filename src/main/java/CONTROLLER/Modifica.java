package CONTROLLER;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import MODEL.Crud;
import MODEL.Permesso;
import MODEL.UtenteDTO;

/**
 * Servlet implementation class Modifica
 */
@WebServlet("/Modifica")
public class Modifica extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Modifica() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session=request.getSession();
		Crud c=new Crud();
		String cf= request.getParameter("codiceFiscale");
		UtenteDTO u= c.ricercautente(cf);	
		session.getAttribute("login");
		ArrayList<Permesso> array= c.recuperaperm();
		
		if(u!=null) {
			RequestDispatcher rd = request.getRequestDispatcher("insform.jsp");
			request.setAttribute("dip", u);
			request.setAttribute("permessi", array);
			rd.forward(request,response);
		}else {
			RequestDispatcher rd = request.getRequestDispatcher("Gestisci");
			request.setAttribute("msg", "qualcosa è andato storto");
			rd.forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String cf = request.getParameter("oldcf");
		Crud c = new Crud();
		UtenteDTO u=new UtenteDTO();
		
		u.setNome(request.getParameter("nome"));
		u.setCognome(request.getParameter("cognome"));
		u.setSesso(request.getParameter("sesso"));
		u.setDataNascita(c.inserisciData(request.getParameter("dataNascita")));
		u.setCodiceFiscale(request.getParameter("codiceFiscale"));
		u.setCittaResidenza(request.getParameter("cittaResidenza"));
		u.setStipendio(Double.parseDouble(request.getParameter("stipendio")));
		u.setUsername(request.getParameter("username"));
		u.setEmail(request.getParameter("email"));
		u.setPassword(request.getParameter("password"));
		u.setIdPermesso(Integer.parseInt(request.getParameter("idPermesso")));
		
		if(c.modifica(cf, u)>0) {
			RequestDispatcher rd=request.getRequestDispatcher("Gestisci"); 
			request.setAttribute("msg", "Utente modificato correttamente!");
		    rd.forward(request, response);
		}else {
			RequestDispatcher rd=request.getRequestDispatcher("Gestisci"); 
			request.setAttribute("msg", "qualcosa è andato storto");
		    rd.forward(request, response); 
		}
	}

}
