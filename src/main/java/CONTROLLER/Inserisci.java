package CONTROLLER;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import MODEL.Account;
import MODEL.Crud;
import MODEL.Dipendente;
import MODEL.Permesso;
import MODEL.Persona;

/**
 * Servlet implementation class Inserisci
 */
@WebServlet("/Inserisci")
public class Inserisci extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Inserisci() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Crud c= new Crud();
		ArrayList<Permesso> array= c.recuperaperm();
		HttpSession session = request.getSession();
		
		RequestDispatcher rd = request.getRequestDispatcher("insform.jsp"); 
		session.getAttribute("login");
		request.setAttribute("permessi", array);
		rd.forward(request, response); 
	
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Crud c=new Crud();
		Persona p=new Persona();
		Dipendente d=new Dipendente();
		Account a=new Account();
		
		p.setNome(request.getParameter("nome"));
		p.setCognome(request.getParameter("cognome"));
		p.setCodiceFiscale(request.getParameter("codiceFiscale"));
		p.setSesso(request.getParameter("sesso"));
		p.setDataNascita(c.inserisciData(request.getParameter("dataNascita")));
		p.setCittaResidenza(request.getParameter("cittaResidenza"));
		
		if(c.inserimento(p)>0) {
			int idpersona= c.ricercaId(p.getCodiceFiscale());
			d.setStipendio(Double.parseDouble(request.getParameter("stipendio")));
			d.setIdPersona(idpersona);
			a.setUsername(request.getParameter("username"));
			a.setEmail(request.getParameter("email"));
			a.setPassword(request.getParameter("password"));
			a.setIdPermesso(Integer.parseInt(request.getParameter("idPermesso")));
			a.setIdPersona(idpersona);
			if(c.inserimento(d,idpersona)>0 && c.inserimento(a,idpersona)>0) {
				RequestDispatcher rd=request.getRequestDispatcher("Gestisci"); 
				request.setAttribute("msg", "Utente inserito correttarmente!");
			    rd.forward(request, response);
			}else {
				RequestDispatcher rd=request.getRequestDispatcher("gestioneazienda.jsp"); 
				request.setAttribute("msg", "qualcosa è andato storto");
			    rd.forward(request, response); 
			}
		}else {
			RequestDispatcher rd=request.getRequestDispatcher("gestioneazienda.jsp"); 
			request.setAttribute("msg", "qualcosa è andato storto");
		    rd.forward(request, response); 
		 
		}
		
	}

}
