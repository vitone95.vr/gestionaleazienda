package CONTROLLER;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import MODEL.CrudP;
import MODEL.Progetto;

/**
 * Servlet implementation class Ricercacosto
 */
@WebServlet("/Ricercacosto")
public class Ricercacosto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ricercacosto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		CrudP c=new CrudP();
		String costo=request.getParameter("rcosto");
		ArrayList<Progetto> progetti= null;
		
		if(Integer.parseInt(costo)>=0 && costo!=null && costo!="") {
			if(c.ricercacosto(Integer.parseInt(costo))!=null) {
				progetti=c.ricercacosto(Integer.parseInt(costo));
				request.setAttribute("progetti", progetti);
				RequestDispatcher rd = request.getRequestDispatcher("ricercaprog.jsp"); 
				rd.forward(request, response);
			}else {
				RequestDispatcher rd=request.getRequestDispatcher("Gestisciprog");
				request.setAttribute("msg","progetti non trovati tramite costo!");
				rd.forward(request,response);
			}
		}else {
			RequestDispatcher rd=request.getRequestDispatcher("Gestisciprog");
			request.setAttribute("msg","non hai inserito nulla nei campi o qualcosa è andato storto!");
			rd.forward(request,response);
		}
	}

}
