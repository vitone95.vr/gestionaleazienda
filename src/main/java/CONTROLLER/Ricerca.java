package CONTROLLER;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import MODEL.Crud;
import MODEL.UtenteDTO;

/**
 * Servlet implementation class Ricerca
 */
@WebServlet("/Ricerca")
public class Ricerca extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ricerca() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Crud c=new Crud();
		String cf = request.getParameter("rcodiceFiscale");
		String cr = request.getParameter("rcittaResidenza");
		String nome = request.getParameter("rnome");
		String cognome = request.getParameter("rcognome");
		ArrayList<UtenteDTO> u=null;
		
		if(cf!=null && cf!="") {
			if (c.ricercautente(cf)!=null) {
				u=c.ricercautenti(cf);
				request.setAttribute("utenti", u);
				RequestDispatcher rd = request.getRequestDispatcher("ricerca.jsp"); 
				rd.forward(request, response);
			}else {
				RequestDispatcher rd=request.getRequestDispatcher("Gestisci");
				request.setAttribute("msg","utente non trovato tramite cf!");
				rd.forward(request,response);
			}
		}else if (cr!=null && cr!="") {
			if (c.ricercautenti2(cr)!=null) {
				u=c.ricercautenti2(cr);
				request.setAttribute("utenti", u);
				RequestDispatcher rd = request.getRequestDispatcher("ricerca.jsp"); 
				rd.forward(request, response);
			}else {
				RequestDispatcher rd=request.getRequestDispatcher("Gestisci");
				request.setAttribute("msg","utente non trovato tramite residenza!");
				rd.forward(request,response);
			}
		}else if ((nome!="" && cognome!="") && (nome!=null && cognome!=null)) {
			if (c.ricercautenti3(nome,cognome)!=null) {
				u=c.ricercautenti3(nome,cognome);
				request.setAttribute("utenti", u);
				RequestDispatcher rd = request.getRequestDispatcher("ricerca.jsp"); 
				rd.forward(request, response);
			}else {
				RequestDispatcher rd=request.getRequestDispatcher("Gestisci");
				request.setAttribute("msg","utente non trovato tramite nome e cognome!");
				rd.forward(request,response);
			}
		}else {
			RequestDispatcher rd=request.getRequestDispatcher("Gestisci");
			request.setAttribute("msg","non hai inserito nulla nei campi o qualcosa è andato storto!");
			rd.forward(request,response);
		}
		
	}

}
