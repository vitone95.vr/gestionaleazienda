package CONTROLLER;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import MODEL.CrudP;
import MODEL.Progetto;
import MODEL.UtenteDTO;

/**
 * Servlet implementation class Dettagliprog
 */
@WebServlet("/Dettagliprog")
public class Dettagliprog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Dettagliprog() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getSession().getAttribute("login") == null) {
			response.sendRedirect("Login");
		}else {
			CrudP c= new CrudP();
			String n= request.getParameter("nomeProgetto");
			Progetto p= c.ricerca(n);
			ArrayList<UtenteDTO> dipin = c.recuperadipin(n);
			ArrayList<UtenteDTO> dipout = c.recuperadipout(n);		
			RequestDispatcher rd = request.getRequestDispatcher("dettagliprog.jsp"); 
			request.setAttribute("prog", p);
			request.setAttribute("dipin", dipin);
			request.setAttribute("dipout", dipout);
		    rd.forward(request, response); 
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
