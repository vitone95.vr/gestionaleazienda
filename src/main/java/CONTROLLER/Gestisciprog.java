package CONTROLLER;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import MODEL.CrudP;
import MODEL.Progetto;
import MODEL.Validator;

/**
 * Servlet implementation class Gestisciprog
 */
@WebServlet("/Gestisciprog")
public class Gestisciprog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Gestisciprog() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getSession().getAttribute("login") == null) {
			response.sendRedirect("Login");
		}else {			
			CrudP c=new CrudP();
			ArrayList<Progetto> progetti=c.recuperaprog();
			int pagineFilter;
			if (request.getParameter("pagineFilter") == null) {
				pagineFilter = 5;
			}else {				
				pagineFilter = Integer.parseInt(request.getParameter("pagineFilter"));
			}
			int pagine = (int) Math.ceil((progetti.size()) / (double) pagineFilter);
			int offset =((Validator.isNumber(request.getParameter("page"))) ? (Integer.parseInt(request.getParameter("page"))-1)*pagineFilter : 0);
			
			if(progetti.size()>0) {
				HttpSession session = request.getSession();
				RequestDispatcher rd = request.getRequestDispatcher("ricercaprog.jsp"); 
				session.getAttribute("login");
				request.setAttribute("pagineFilter", pagineFilter);
				request.setAttribute("pagine", pagine);
				request.setAttribute("progetti", c.recuperaprog(pagineFilter,offset));
				rd.forward(request, response); 
			}else {
				RequestDispatcher rd=request.getRequestDispatcher("gestioneazienda.jsp");
				request.setAttribute("msg","qualcosa è andato storto o non ci sono progetti!");
				rd.forward(request,response);
			}
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
