package CONTROLLER;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import MODEL.Crud;
import MODEL.Persona;

/**
 * Servlet implementation class Elimina
 */
@WebServlet("/Elimina")
public class Elimina extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Elimina() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Crud c= new Crud();
		String cf= request.getParameter("codiceFiscale");
		Persona p=c.ricerca(cf);
		
		if(p!=null) {
			if(c.elimina(p)>0) {
				RequestDispatcher rd=request.getRequestDispatcher("Gestisci"); 
				request.setAttribute("msg", "utente eliminato correttamete!");
			    rd.forward(request, response);   
			}else {
				RequestDispatcher rd=request.getRequestDispatcher("Gestisci"); 
				request.setAttribute("msg", "qualcosa è andato storto!");
			    rd.forward(request, response); 
			}
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
