package CONTROLLER;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import MODEL.CrudP;
import MODEL.Progetto;

/**
 * Servlet implementation class Eliminaprog
 */
@WebServlet("/Eliminaprog")
public class Eliminaprog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Eliminaprog() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CrudP c= new CrudP();
		String n= request.getParameter("nomeProgetto");
		Progetto p=c.ricerca(n);
		
		if(p!=null) {
			if(c.elimina(p)>0) {
				RequestDispatcher rd=request.getRequestDispatcher("Gestisciprog"); 
				request.setAttribute("msg", "progetto eliminato correttamete!");
			    rd.forward(request, response);   
			}else {
				RequestDispatcher rd=request.getRequestDispatcher("Gestisciprog"); 
				request.setAttribute("msg", "qualcosa è andato storto!");
			    rd.forward(request, response); 
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
