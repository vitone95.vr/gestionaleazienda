<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

<title>Tutti i progetti</title>
</head>
<body class="bg-dark-subtle">

	<div class="container-fluid">
	<p class="text-center bg-warning msg">Benvenuto ${login.username}</p>
	<c:if test="${msg!=null}">
		<p class="text-center bg-primary" id="msg">${msg}</p>
	</c:if>
		<nav class="navbar navbar-expand-lg bg-body-tertiary">
	    	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	      		<ul class="navbar-nav me-auto mb-2 mb-lg-0">
	      			<li>
	        			<a class="nav-link disabled text-black">Cerca per costo maggiore o uguale:</a>
	      			</li>
	      			<li>
	      				<form class="d-flex" role="search" action="Ricercacosto" method="post">
	        				<input class="form-control me-2" type="search" placeholder="Inserisci costo" aria-label="Search" name="rcosto">
	        				<button class="btn btn-outline-success" type="submit">Cerca</button>
	      				</form>
	      			</li>
					<li>
						<a class="nav-link disabled text-black">Filtra</a>
					</li>
					<li>
						<form class="d-flex" role="search" action="Gestisciprog" method="get">
							<input type="number" class="form-control me-2"
								name="pagineFilter" value="${pagineFilter}">
							<button type="submit" class="btn btn-outline-success">Invia</button>
						</form>
					</li>
				</ul>
	    	</div>
		</nav>
	
		<h1>Progetti inseriti:</h1>
		<div class="row">
			<div class="col-9">
				<table class="table bg-secondary-subtle">
					<thead>
						<tr>
							<th scope="col">NOME PROGETTO</th>
							<th scope="col">DESCRIZIONE</th>
							<th scope="col">IMG</th>
							<th scope="col">COSTO</th>
							<th scope="col">NUM DIP</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="prog" items="${progetti}">
							<tr>
								<td>${prog.nomeProgetto}</td>
								<td>${prog.descrizione}</td>
								<td>${prog.linkimg}</td>
								<td>${prog.costo}</td>
						  	    <td>${prog.numdip}</td> 
								
								<td><a href="Dettagliprog?nomeProgetto=${prog.nomeProgetto}" class="btn btn-info ${login.getIdPermesso()==2 ? 'd-inline' : 'd-none'}"><b>Dettagli</b></a></td>
								<td><a href="Modificaprog?nomeProgetto=${prog.nomeProgetto}" class="btn btn-warning ${login.getIdPermesso()==2 ? 'd-inline' : 'd-none'}"><b>Modifica</b></a></td>
								<td><a href="Eliminaaprog?nomeProgetto=${prog.nomeProgetto}" class="btn btn-danger ${login.getIdPermesso()==2 ? 'd-inline' : 'd-none'} ${prog.numdip==0 ? 'd-inline' : 'd-none'}"><b>Elimina</b></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
			<div class="col-3 mb-3">
				<a href="gestioneazienda.jsp" class="btn btn-primary text-center">Torna alla gestione azienda</a>
			</div>
		<nav>
			<ul class="pagination">
				<c:choose>
					<c:when test="${param.page==null || param.page==1}">
						<li class="page-item disabled"><a class="page-link btn">Indietro</a></li>
					</c:when>
					<c:otherwise>
						<li class="page-item cursor-pointer"><a class="page-link btn btn-outline-primary"
							onclick="paginate('back')">Indietro</a></li>
					</c:otherwise>
				</c:choose>

				<c:forEach var="i" begin="1" end="${ pagine }">
					<c:choose>
						<c:when test="${ (i==1 && param.page==null)}">
							<li class="page-item cursor-pointer active"><a
								class="page-link" onclick="paginate('${ i }')">${ i }</a></li>
						</c:when>
						<c:when test="${i==param.page }">
							<li class="page-item cursor-pointer active"><a
								class="page-link" onclick="paginate('${ i }')">${ i }</a></li>
						</c:when>
						<c:otherwise>
							<li class="page-item cursor-pointer"><a class="page-link"
								onclick="paginate('${ i }')">${ i }</a></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>

				<c:choose>
					<c:when test="${param.page==pagine || pagine == 1}">
						<li class="page-item disabled"><a class="page-link btn">Avanti</a></li>
					</c:when>
					<c:otherwise>
						<li class="page-item cursor-pointer"><a class="page-link btn btn-outline-primary"
							onclick="paginate('next')">Avanti</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</nav>
	</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="myscript.js"></script>
</body>
</html>