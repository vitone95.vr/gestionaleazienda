<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
<script type="text/javascript">
		  var isLoggedIn = "${sessionScope.login.getUsername() != null}";
		  if(isLoggedIn !== "true")
		     window.location.href="Login";
</script>
<title>Gestisci</title>
</head>
<body class="bg-dark-subtle">

	<div class="container-fluid">
		
		<p class="text-center bg-warning">Benvenuto ${login.username}<a href="Logout" class="btn btn-danger">Logout</a></p>
		<c:if test="${msg!=null}">
			<p class="text-center bg-primary" id="msg">${msg}</p>
		</c:if>
		
		<h1>Gestione azienda</h1>
	
		<a href="Gestisci" class="btn btn-primary text-center">${login.getIdPermesso()==2 ? 'Gestisci' : 'Visualizza'} Dipendenti</a>
		<a href="Inserisci" class="btn btn-primary text-center ${login.getIdPermesso()==2 ? 'd-inline' : 'd-none'}">Inserisci Dipendente</a>
	
		<a href="insprog.jsp" class="btn btn-primary text-center ${login.getIdPermesso()==2 ? 'd-inline' : 'd-none'}">Inserisci Progetto</a>	
		<a href="Gestisciprog" class="btn btn-primary text-center">${login.getIdPermesso()==2 ? 'Gestisci' : 'Visualizza'} Progetti</a>
	
	</div>
	
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="myscript.js"></script>
</body>
</html>