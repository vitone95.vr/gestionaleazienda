$("[href!='gestioneazienda.jsp']").ready(function() {
	$("#msg").fadeOut(3000);  
});

function showpass() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function paginate(page) {
	var url = new URL(window.location.href);
	if (page == "next" || page == "back") {
		var current = url.searchParams.get('page');
		if (page == "next") 
			url.searchParams.set('page', ((current == null) ? 2 : (Number(current)+1)));
		else
			url.searchParams.set('page', ((current == null) ? 1 : (Number(current)-1)));
	}else{
		url.searchParams.set('page', page);
	}
	
	window.location.href=url;
}