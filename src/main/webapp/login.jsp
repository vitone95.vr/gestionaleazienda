<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
<title>Login</title>
</head>
<body class="bg-dark">

<div class="container-fluid">
	<h1 class="text-light text-center">Accedi alla piattaforma</h1>
	<c:if test="${msg!=null}">
		<p class="text-center bg-danger" id="msg">${msg}</p>
	</c:if>

		<div class="row d-flex justify-content-center">
			<div class="col-3">
				<form method="post" action="Login" class="bg-secondary p-3">
					<div class="mb-3">
						<label for="exampleInputEmail1" class="form-label">Inserisci email o username</label>
						 <input type="text" class="form-control" id="exampleInputEmail1" name="username">
					</div>
					<div class="mb-3">
						<label class="form-label">Password</label>
						 <input type="password"class="form-control" id="password" name="password">
					</div>
					<div class="mb-3 form-check">
						<input type="checkbox" class="form-check-input" id="exampleCheck1" onclick="showpass()">
						 <label class="form-check-label">mostra password</label>
					</div>
					<button type="submit" class="btn btn-primary">Invia</button>
				</form>
			</div>
		</div>

</div>

	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="myscript.js"></script>
</body>
</html>