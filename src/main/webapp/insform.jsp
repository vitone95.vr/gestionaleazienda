<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
<title>Insert title here</title>
</head>
<body class="bg-dark-subtle">

	<div class="container-fluid">
		<p class="text-center bg-warning msg">Benvenuto ${login.username}</p>
		<h1 class="text-center">Inserisci dipendente</h1>
		<div class="row d-flex justify-content-center">
			<div class="col-3">
				<form method="post" action="${dip.getCodiceFiscale()==null ? 'Inserisci' : 'Modifica'}" class="bg-black p-3" id="myform">
					<div class="mb-3">
						<label class="form-label text-light">Nome</label> <input
							type="text" class="form-control" placeholder="Inserisci nome"
							id="nome" name="nome" value="${dip.nome}">
						<!-- onblur="return validatename()" -->
						<div id="errname" class="form-text"></div>
					</div>
					<div class="mb-3">
						<label class="form-label text-light">Cognome</label> <input
							type="text" class="form-control" placeholder="Inserisci cognome"
							id="cognome" value="${dip.cognome}" name="cognome">
						<!-- onblur="return validatesurname()" -->
						<div id="errsurname" class="form-text"></div>
					</div>
					<div class="mb-3">
						<label class="form-label text-light">Sesso</label>
						<br>
						<div class="form-check form-check-inline">
							<label class="form-check-label text-light"> 
							<input class="form-check-input" type="radio" name="sesso"  value="m" checked> Uomo
							</label>
						</div>
						<div class="form-check form-check-inline">
							<label class="form-check-label text-light"> 
							<input class="form-check-input" type="radio" name="sesso"  value="f"> Donna
							</label>
						</div>
					</div>
					<div class="col-md-6">
                        <span class="text-light">Data di nascita</span>
                        <input type="date" class="form-control"  name="dataNascita" value="${dip.dataNascita}"> <!-- onchange="validate('dataNascita')" -->
                        <div id="errdataNascita" class="form-text"></div>
                    </div>
					<div class="mb-3">
						<label class="form-label text-light">Codice fiscale</label> <input
							type="text" class="form-control" placeholder="Inserisci codice fiscale"
							id="codiceFiscale" value="${dip.codiceFiscale}" name="codiceFiscale">
							<input type="hidden" name="oldcf" value="${dip.codiceFiscale}">
						<!--onchange="return validatecf()"  -->
						<div id="errcodiceFiscale" class="form-text"></div>
					</div>
					<div class="mb-3">
						<label class="form-label text-light">Citt� di residenza</label> <input
							type="text" class="form-control" placeholder="Inserisci citt� di residenza"
							id="cittaResidenza" value="${dip.cittaResidenza}" name="cittaResidenza">
						<!-- onblur="return validateeta()" -->
						<div id="errcittaResidenza" class="form-text"></div>
					</div>
					<div class="mb-3">
						<label class="form-label text-light">Stipendio</label> <input
							type="number" class="form-control"
							placeholder="Insersci stipendio" id="stipendio"
							value="${dip.stipendio}" name="stipendio">
						<!-- onblur="return validatestip()" -->
						<div id="errstip" class="form-text"></div>
					</div>
					<div class="mb-3">
						<label class="form-label text-light">Username</label> <input
							type="text" class="form-control" placeholder="Inserisci username"
							id="username" name="username" value="${dip.username}">
						<!-- onblur="return validatename()" -->
						<div id="errusername" class="form-text"></div>
					</div>
					<div class="mb-3">
                        <label class="form-label text-light">Indirizzo e-mail</label>
                        <input type="email" class="form-control" id="email" name="email" value="${dip.email}" placeholder="inserisci email"> <!-- onblur="return validateemail()" -->
                        <div id="erremail" class="form-text"></div>
                    </div>
					<div class="mb-3">
                        <label class="form-label text-light">Password</label>
                        <input type="password" class="form-control" name="password" id="password" value="${dip.password}" placeholder="inserisci password"> <!-- onblur="return validatepassword()" -->
                        <div id="errpassword" class="form-text"></div>
                        <div class="mb-3 form-check">
							<input type="checkbox" class="form-check-input" id="exampleCheck1" onclick="showpass()">
						 	<label class="form-check-label text-light">mostra password </label>
						</div>
                        <ul id="passrules">
                            <li><p class="text-light">La password deve contenere: </p></li>
                            <li><p class="text-light">Almeno 1 lettera maiuscola</p></li>
                            <li><p class="text-light">Almeno 1 lettera minuscola</p></li>
                            <li><p class="text-light">Almeno 1 numero</p></li>
                            <li><p class="text-light">Min 6 caratteri</p></li>
                            <li><p class="text-light">Max 20 caratteri</p></li>
                        </ul>
                    </div>
                    <div class="mb-3">
                        <label class="form-label text-light">Permesso</label>
                        <select name="idPermesso" class="form-select">
  							<option>Seleziona permesso</option>
  							<c:forEach var="perm" items="${permessi}">
  							<option value="${perm.id}" ${dip.idPermesso==perm.id ? 'selected' : ''}>${perm.tipo}</option>
  							</c:forEach>
						</select>
                    </div>
                    
					<button type="submit" class="btn btn-warning" id="sub">Invia</button>
					<!-- onclick="return validate()" -->
					<a href="${dip.getCodiceFiscale()==null ? 'gestioneazienda.jsp' : 'Gestisci'}" class="btn btn-primary text-center">Torna indietro</a>
				</form>
			</div>
		</div>

	</div>

	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="myscript.js"></script>
</body>
</html>