<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

<title>Dettagli progetto</title>
</head>
<body class="bg-dark-subtle">

	<div class="container-fluid">
	<p class="text-center bg-primary" id="msg">${msg}</p>
	
		<h1>Dipendenti assegnati:</h1>
		<div class="row">
			<div class="col-9">
				<table class="table bg-secondary-subtle">
					<thead>
						<tr>
							<th scope="col">NOME </th>
							<th scope="col">COGNOME</th>
							<th scope="col">STIPENDIO</th>
							<th scope="col">EMAIL</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="dipin" items="${dipin}">
							<tr>
								<td>${dipin.nome}</td>
								<td>${dipin.cognome}</td>
								<td>${dipin.stipendio}</td>
								<td>${dipin.email}</td>
						  	    
								<td><a href="Dissocia?iddip=${dipin.id}&idprog=${prog.getId()}&nomeProgetto=${prog.getNomeProgetto()}" class="btn btn-info"><b>Dissocia</b></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<h1>Dipendenti non assegnati:</h1>
		<div class="row">
			<div class="col-9">
				<table class="table bg-secondary-subtle">
					<thead>
						<tr>
							<th scope="col">NOME </th>
							<th scope="col">COGNOME</th>
							<th scope="col">STIPENDIO</th>
							<th scope="col">EMAIL</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="dipout" items="${dipout}">
							<tr>
								<td>${dipout.nome}</td>
								<td>${dipout.cognome}</td>
								<td>${dipout.stipendio}</td>
								<td>${dipout.email}</td>
						  	    
								<td><a href="Associa?iddip=${dipout.id}&idprog=${prog.getId()}&nomeProgetto=${prog.getNomeProgetto()}" class="btn btn-info"><b>Associa</b></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<a href="Gestisciprog" class="btn btn-primary text-center">Torna ai progetti</a>
			</div>
		</div>
	
	</div>


<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="myscript.js"></script>
</body>
</html>