<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
<title>Lista utenti</title>
</head>
<body class="bg-secondary">

	<div class="container-fluid">
	<p class="text-center bg-warning msg">Benvenuto ${login.username}</p>
	<c:if test="${msg!=null}">
		<p class="text-center bg-primary" id="msg">${msg}</p>
	</c:if>

	<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
      <li>
        	<a class="nav-link disabled text-black">Cerca per cf</a>
        </li>
      	<li>
      		<form class="d-flex" role="search" action="Ricerca" method="post">
        		<input class="form-control me-2" type="search" placeholder="Inserisci codice fiscale" aria-label="Search" name="rcodiceFiscale">
        		<button class="btn btn-outline-success" type="submit">Cerca</button>
      		</form>
      	</li>
      	<li>
        	<a class="nav-link disabled text-black">Cerca per zona residenza</a>
        </li>
      	<li>
      		<form class="d-flex" role="search" action="Ricerca" method="post">
        		<input class="form-control me-2" type="search" placeholder="Inserisci zona di residenza" aria-label="Search" name="rcittaResidenza">
        		<button class="btn btn-outline-success" type="submit">Cerca</button>
      		</form>
      	</li>
      	<li>
        	<a class="nav-link disabled text-black">Cerca per nome e cognome</a>
        </li>
        <li>
            <form class="d-flex" role="search" action="Ricerca" method="post">
		        <input class="form-control me-2" type="search" placeholder="Inserisci nome" aria-label="Search" name="rnome">
		        <input class="form-control me-2" type="search" placeholder="Inserisci cognome" aria-label="Search" name="rcognome">        			
        		<button class="btn btn-outline-success" type="submit">Cerca</button>
      		</form>
      	</li>
      	<li>
        	<a class="nav-link disabled text-black">Filtra</a>
        </li>
      	<li>
      		<form class="d-flex" role="search" action="Gestisci" method="get">
				<input type="number" class="form-control me-2" name="pagineFilter" value="${pagineFilter}">
				<button type="submit" class="btn btn-outline-success">Invia</button>
			</form>
		</li>
       </ul>
    </div>
	</nav>

	<h1>Utenti inseriti:</h1>
		<div class="row">
			<div class="col-9">
				<table class="table bg-secondary-subtle">
					<thead>
						<tr>
							<th scope="col">NOME</th>
							<th scope="col">COGNOME</th>
							<th scope="col">CF</th>
							<th scope="col">STIPENDIO</th>
							<th scope="col">PERMESSO</th>
							<th scope="col">NUM PROGETTI</th>
							<%-- <th scope="col" class="${login.getIdPermesso()==2 ? 'd-block' : 'd-none'}">MODIFICA</th>
							<th scope="col" class="${login.getIdPermesso()==2 ? 'd-block' : 'd-none'}">ELIMINA</th>
							<th scope="col">DETTAGLI</th> --%>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="utente" items="${utenti}">
							<tr>
								<td>${utente.nome}</td>
								<td>${utente.cognome}</td>
								<td>${utente.codiceFiscale}</td>
								<td>${utente.stipendio}</td>
								<td>${utente.tipo}</td>
								<td>${utente.numprog}</td>
								
								<td>
								<!-- Button trigger modal DETTAGLI-->
								<button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal${utente.id}">Dettagli</button>
								<!-- Modal -->							
								<div class="modal fade" id="exampleModal${utente.id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
								  <div class="modal-dialog modal-xl">
								    <div class="modal-content">
								      <div class="modal-header">
								        <h1 class="modal-title fs-5" id="exampleModalLabel">Dettagli</h1>
								        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
								      </div>
								      <div class="modal-body">
								        	<table class="table">
												  <thead>
												    <tr>
												  		<th scope="col">NOME</th>
												  		<th scope="col">COGNOME</th>
												  		<th scope="col">SESSO</th>
												  		<th scope="col">DATA NASCITA</th>
														<th scope="col">CF</th>
												  		<th scope="col">CITTA' RESIDENZA</th>
														<th scope="col">STIPENDIO</th>
														<th scope="col">USERNAME</th>
														<th scope="col">EMAIL</th>
														<th scope="col">PERMESSO</th>
												    </tr>
												  </thead>
												  <tbody>
												    <tr>											      
												      	<td>${utente.nome}</td>
														<td>${utente.cognome}</td>
														<td>${utente.sesso}</td>
														<td>${utente.dataNascita}</td>
														<td>${utente.codiceFiscale}</td>
														<td>${utente.cittaResidenza}</td>
														<td>${utente.stipendio}</td>
														<td>${utente.username}</td>
														<td>${utente.email}</td>
														<td>${utente.tipo}</td>
												    </tr>										   
												  </tbody>
												</table>
								      </div>
								      <div class="modal-footer">
								        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Chiudi</button>
								      </div>
								    </div>
								  </div>
								</div>
								</td>								
								
								<td><a href="Modifica?codiceFiscale=${utente.codiceFiscale}" class="btn btn-warning ${login.getIdPermesso()==2 ? 'd-inline' : 'd-none'}"><b>Modifica</b></a></td>
								
								<td>
								<!-- Button trigger modal ELIMINA-->
								<button type="button" class="btn btn-danger ${login.getIdPermesso()==2 ? 'd-inline' : 'd-none'} ${utente.numprog==0 ? 'd-inline' : 'd-none'}" data-bs-toggle="modal" data-bs-target="#exampleModal">Elimina</button>
								<!-- Modal -->
								<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
									  <div class="modal-dialog">
									    <div class="modal-content">
									      <div class="modal-header">
									        <h1 class="modal-title fs-5" id="exampleModalLabel">Elimina</h1>
									        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
									      </div>
									      <div class="modal-body">
									        <span>Sei sicuro di voler eliminare?</span>
									      </div>
									      <div class="modal-footer">
									        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Chiudi</button>
											<a href="Elimina?codiceFiscale=${utente.codiceFiscale}" class="btn btn-danger">Elimina</a>						        
									      </div>
									    </div>
									  </div>
								</div>
								</td>
								
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
	    </div>
	    <div class="col-3 mb-3">
			<a href="gestioneazienda.jsp" class="btn btn-dark text-light" >Torna a gestione azienda</a>
	    </div>
		<nav>
			<ul class="pagination">
				<c:choose>
					<c:when test="${param.page==null || param.page==1}">
						<li class="page-item disabled"><a class="page-link btn">Indietro</a></li>
					</c:when>
					<c:otherwise>
						<li class="page-item cursor-pointer"><a class="page-link btn btn-outline-primary"
							onclick="paginate('back')">Indietro</a></li>
					</c:otherwise>
				</c:choose>

				<c:forEach var="i" begin="1" end="${ pagine }">
					<c:choose>
						<c:when test="${ (i==1 && param.page==null)}">
							<li class="page-item cursor-pointer active"><a
								class="page-link" onclick="paginate('${ i }')">${ i }</a></li>
						</c:when>
						<c:when test="${i==param.page }">
							<li class="page-item cursor-pointer active"><a
								class="page-link" onclick="paginate('${ i }')">${ i }</a></li>
						</c:when>
						<c:otherwise>
							<li class="page-item cursor-pointer"><a class="page-link"
								onclick="paginate('${ i }')">${ i }</a></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>

				<c:choose>
					<c:when test="${param.page==pagine || pagine == 1}">
						<li class="page-item disabled"><a class="page-link btn">Avanti</a></li>
					</c:when>
					<c:otherwise>
						<li class="page-item cursor-pointer"><a class="page-link btn btn-outline-primary"
							onclick="paginate('next')">Avanti</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</nav>

	</div>
	

	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="myscript.js"></script>
</body>
</html>