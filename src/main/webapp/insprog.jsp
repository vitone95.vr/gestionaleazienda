<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

<title>Inserimento Progetto</title>
</head>
<body class="bg-dark-subtle">
	
	<div class="container-fluid">
	
		<p class="text-center bg-warning msg">Benvenuto ${login.username}</p>
		
		<h1 class="text-center">Inserisci dipendente</h1>
		
		<div class="row d-flex justify-content-center">
			<div class="col-3">
				<form method="post" action="${prog.getNomeProgetto()==null ? 'Inserisciprog' : 'Modificaprog'}" class="bg-black p-3" id="myform">
					<div class="mb-3">
						<label class="form-label text-light">Nome progetto</label> <input
							type="text" class="form-control" placeholder="Inserisci nome progetto"
							id="nomeProgetto" name="nomeProgetto" value="${prog.nomeProgetto}">
							<input type="hidden" name="oldn" value="${prog.nomeProgetto}">
						<!-- onblur="return validatename()" -->
						<div id="errname" class="form-text"></div>
					</div>
					<div class="mb-3">
						<label class="form-label text-light">Descrizione</label> <input
							type="text" class="form-control" placeholder="Inserisci descrizione progetto"
							id="descrizione" value="${prog.descrizione}" name="descrizione">
						<!-- onblur="return validatesurname()" -->
						<div id="errsurname" class="form-text"></div>
					</div>					
					<div class="mb-3">
						<label class="form-label text-light">Immagine</label> <input
							type="text" class="form-control" placeholder="Inserisci immagine"
							id="linkimg" value="${prog.linkimg}" name="linkimg">
						<!--onchange="return validatecf()"  -->
						<div id="errcodiceFiscale" class="form-text"></div>
					</div>
					<div class="mb-3">
						<label class="form-label text-light">Costo</label> <input
							type="number" class="form-control"
							placeholder="Insersci costo progetto" id="costo"
							value="${prog.costo}" name="costo">
						<!-- onblur="return validatestip()" -->
						<div id="errstip" class="form-text"></div>
					</div>					                   
					<button type="submit" class="btn btn-warning" id="sub">Invia</button>
					<!-- onclick="return validate()" -->
					<a href="${prog.getNomeProgetto()==null ? 'gestioneazienda.jsp' : 'Gestisciprog'}" class="btn btn-primary text-center">Torna indietro</a>
				</form>
			</div>
		</div>

	</div>
	
	

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="myscript.js"></script>
</body>
</html>